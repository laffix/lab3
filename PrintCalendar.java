///////////////////////////////////////////////////////////////////////////////////////
// Lab Number: 3
// Author Name: Cesar Arroyo
// Module Description: The program prints out a calendar                            ///
// 
// Date: 2/12/18
//////////////////////////////////////////////////////////////////////////////////////
import java.util.Calendar;
import java.util.Scanner;

public class PrintCalendar {
public static void main(String args[]) {
getYear(args);
System.out.printf("%13d\n",currentYear);
determineLeapYear(currentYear);
printMonth();
printDayNumbers();
}

private static int currentYear;
private static int getYear(String args[]) {
if (args.length==0) 
   currentYear = Calendar.getInstance().get(Calendar.YEAR);
else 
   currentYear = Integer.parseInt(args[0]);
   return currentYear;
   }  
private static int month;
private static void printMonth() {
   month = 0;
   while (month!=12) {
    String monthString;
        switch (month) {
            case 0:  monthString = "January";
                     break;
            case 1:  monthString = "February";
                     break;
            case 2:  monthString = "March";
                     break;
            case 3:  monthString = "April";
                     break;
            case 4:  monthString = "May";
                     break;
            case 5:  monthString = "June";
                     break;
            case 6:  monthString = "July";
                     break;
            case 7:  monthString = "August";
                     break;
            case 8:  monthString = "September";
                     break;
            case 9: monthString = "October";
                     break;
            case 10: monthString = "November";
                     break;
            case 11: monthString = "December";
                     break;
            default: monthString = "Invalid month";
                     break;
        }
        if (currentYear==Calendar.getInstance().get(Calendar.YEAR) && month==Calendar.getInstance().get(Calendar.MONTH))
        {
        System.out.println("----------------------");
        System.out.printf("%14s\n\n",monthString);
        printDaysOfWeek();
        System.out.println("----------------------");
        }
        else {
        System.out.printf("%14s\n\n",monthString);
        printDaysOfWeek();
        }
        month++;
    } 
 }   
 public static void printDaysOfWeek() {
 System.out.println("S  M  Tu  W  Th  F  S");
 }
   
 private static int dayNumber;  
  private static void getNumberOfDay() {
  dayNumber=1;
} 


 private static void printDayNumbers() {
 for (int count=0;count<dayNumber;count++) {
 System.out.print(dayNumber);
 }
 }
 
 // Returns true if it is a leap year
 private static boolean determineLeapYear(int year) {
 if  ((year % 4 == 0) && (year % 100 != 0)) {
      return true;
      }
 else if  (year % 400 == 0)  {
      return true;
      }
 else
      return false;
 }

}